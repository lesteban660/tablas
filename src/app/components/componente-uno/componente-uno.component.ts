import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})

export class ComponenteUnoComponent implements OnInit {

  propiedad3: boolean = true;
  examen: boolean = true;
  numero: any;

  constructor() { }

 

  // objetos = [
  //   { propiedad1: 'objeto1', propiedad2: 1, propiedad3: true, examen: true },
  //   { propiedad1: 'objeto2', propiedad2: 2, propiedad3: false, examen: false },
  //   { propiedad1: 'objeto3', propiedad2: 3, propiedad3: true, examen: true },
  //   { propiedad1: 'objeto4', propiedad2: 4, propiedad3: false, examen: false },
  //   { propiedad1: 'objeto5', propiedad2: 5, propiedad3: true, examen: false },
  //   { propiedad1: 'objeto6', propiedad2: 6, propiedad3: false, examen: true }
  // ];

  // objetoAnidado = {
  //   id: 1,
  //   nombre: 'Objeto 1',
  //   subobjeto: {
  //     id: 11,
  //     nombre: 'Subobjeto 1',
  //     subsubobjeto: {
  //       id: 111,
  //       nombre: 'Subsubobjeto 1'
  //     }
  //   }
  // };

  // numeros: number[] = [];

  // generarTabla(numero: number) {
  //   this.numeros = [];
  //   for (let i = 1; i <= 10; i++) {
  //     this.numeros.push(numero * i);
  //   }
  // }

  ngOnInit(): void {
  }

}
